// tree-grid使用
var $ModalChange = $('#exampleModalChange');
var $treeTable = $('#tb_departments');
$treeTable.bootstrapTable({

    classes: 'table table-bordered table-hover table-striped',
    url: '/admin/attach/datalist',
    method: 'get',
    dataType: 'json', // 因为本示例中是跨域的调用,所以涉及到ajax都采用jsonp,
    uniqueId: 'id',
    idField: 'id', // 每行的唯一标识字段
    toolbar: '#toolbar2', // 工具按钮容器
    //clickToSelect: true,     // 是否启用点击选中行
    showColumns: true, // 是否显示所有的列
    showToggle: true, // 是否显示详细视图和列表视图的切换按钮(clickToSelect同时设置为true时点击会报错)
    pagination: true, // 是否显示分页
    sortOrder: "asc", // 排序方式

    columns: [
        {
            field: 'check',
            checkbox: true
        },
        {
            field: 'url',
            title: '预览',
            formatter: function(value, row, index) {
                return '<img src="'+ value +'" class="rounded" style="width: 100px;height: 100px;" ">'
            },
        },
        {
            field: 'filesize',
            title: '文件大小'
        },
        {
            field: 'filetype',
            title: '文件类型'
        },
        {
            field: 'storage',
            title: '存储引擎'
        },
        {
            field: 'username.nickname',
            title: '上传者'
        },
        {
            field: 'create_time',
            title: '上传时间'
        },
        {
            field: 'operate',
            title: '操作',
            align: 'center',
            events: {
                'click .role-delete': function(e, value, row, index) {
                    del(row.id);
                },
            },
            formatter: operateFormatter
        }
    ],

    responseHandler: function(res) {
        return {
            //"total": res.DATA.totalRecordCount,//总页数
            "rows": res.data.data   //数据
        };
    },

    treeShowField: 'name',
    parentIdField: 'pid',
    search:false,  //是否启用搜索框
    searchOnEnterKey:false,  //按回车触发搜索方法
    showRefresh: true,  //是否显示刷新按钮

});

// 操作按钮
function operateFormatter(value, row, index) {
    return [
        '<a type="button" class="role-delete btn btn-xs btn-default" title="删除" data-toggle="tooltip"><i class="mdi mdi-delete"></i></a>'
    ].join('');
}

/**
 * 选中父项时，同时选中子项
 * @param datas 所有的数据
 * @param row 当前数据
 * @param id id 字段名
 * @param pid 父id字段名
 */
function selectChilds(datas, row, id, pid, checked) {
    for (var i in datas) {
        if (datas[i][pid] == row[id]) {
            datas[i].check = checked;
            selectChilds(datas, datas[i], id, pid, checked);
        };
    }
}

function selectParentChecked(datas, row, id, pid) {
    for (var i in datas) {
        if (datas[i][id] == row[pid]) {
            datas[i].check = true;
            selectParentChecked(datas, datas[i], id, pid);
        };
    }
}

function post_submit() {
    $.ajax({
        //几个参数需要注意一下
        type: "POST",//方法类型
        dataType: "json",//预期服务器返回的数据类型
        url: "/admin/attach/post_submit" ,//url
        data: $('#form1').serialize(),
        success: function (res) {
            console.log(res);//打印服务端返回的数据(调试用)
            if (res.code === 0) {
                $.alert({
                    title: '成功',
                    icon: 'mdi mdi-alert',
                    type: 'blue',
                    content: res.msg,
                    buttons: {
                        okay: {
                            text: '确认',
                            btnClass: 'btn-blue',
                            action: function() {
                                $ModalChange.modal('hide');
                                $treeTable.bootstrapTable('refresh');
                            }
                        }
                    }
                });
            }else {
                $.alert({
                    title: '错误',
                    icon: 'mdi mdi-alert',
                    type: 'red',
                    content: res.msg,
                    buttons: {
                        okay: {
                            text: '确认',
                            btnClass: 'btn-blue',
                            action: function() {
                                //$('[alt="captcha"]').click();
                            }
                        }
                    }
                });
            };
        },
        error : function() {
            $.alert({
                title: '警告',
                content: '请检查返回是否正常！',
                icon: 'mdi mdi-access-point-network-off',
                animation: 'scale',
                closeAnimation: 'scale',
                buttons: {
                    okay: {
                        text: '确认',
                        btnClass: 'btn-blue'
                    }
                }
            });
        }
    });
}
function add(id) {
    parent.$(parent.document).data('multitabs').create({
        iframe : true,                                // 指定为iframe模式，当值为false的时候，为智能模式，自动判断（内网用ajax，外网用iframe）。缺省为false。
        title : '添加节点',                     // 标题（可选），没有则显示网址
        url : '/admin/attach/authlist?id=' + id                    // 链接（必须），如为外链，强制为info页
    }, true);
}
function del(id) {
    $.confirm({
        title: '提示',
        content: '您确定要删除此角色？',
        icon: 'mdi mdi-alert',
        animation: 'scale',
        closeAnimation: 'zoom',
        buttons: {
            confirm: {
                text: '是的',
                btnClass: 'btn-orange',
                action: function() {
                    $.ajax({
                        //几个参数需要注意一下
                        type: "POST",//方法类型
                        dataType: "json",//预期服务器返回的数据类型
                        url: "/admin/attach/del" ,//url
                        data: {id: id},
                        success: function (res) {
                            console.log(res);//打印服务端返回的数据(调试用)
                            if (res.code === 0) {
                                $.alert({
                                    title: '成功',
                                    icon: 'mdi mdi-alert',
                                    type: 'blue',
                                    content: res.msg,
                                    buttons: {
                                        okay: {
                                            text: '确认',
                                            btnClass: 'btn-blue',
                                            action: function() {
                                                $treeTable.bootstrapTable('refresh');
                                            }
                                        }
                                    }
                                });
                            }else {
                                $.alert({
                                    title: '错误',
                                    icon: 'mdi mdi-alert',
                                    type: 'red',
                                    content: res.msg,
                                    buttons: {
                                        okay: {
                                            text: '确认',
                                            btnClass: 'btn-blue',
                                            action: function() {
                                                //$('[alt="captcha"]').click();
                                            }
                                        }
                                    }
                                });
                            };
                        },
                        error : function() {
                            $.alert({
                                title: '警告',
                                content: '请检查返回是否正常！',
                                icon: 'mdi mdi-access-point-network-off',
                                animation: 'scale',
                                closeAnimation: 'scale',
                                buttons: {
                                    okay: {
                                        text: '确认',
                                        btnClass: 'btn-blue'
                                    }
                                }
                            });
                        }
                    });
                }
            },
            '取消': function() {

            }
        }
    });

}

$('#exampleModalChange').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var recipient = button.data('whatever');
    var modal = $(this);
    if(recipient != 0){
        var data = {
            id:recipient
        }
        $.ajax({
            url: '/admin/attach/index',
            data:data,
            type: 'POST',
            dataType: 'json'
        }).done(function(response) {
            modal.find('.modal-title').text('编辑' + response.data.title);
            modal.find('.modal-body [name=id]').val(recipient);
            modal.find('.modal-body [name=title]').val(response.data.title);
            modal.find('.modal-body [name=comments]').val(response.data.comments);

        }).fail(function() {
            console.error('数据错误');
        });
    }else{
        modal.find('.modal-title').text('新增角色');
        modal.find('.modal-body [name=id]').val('0');
        modal.find('.modal-body [name=title]').val('');
        modal.find('.modal-body [name=comments]').val('');
    }
})

function test() {
    var selRows = $treeTable.bootstrapTable("getSelections");
    if (selRows.length == 0) {
        alert("请至少选择一行");
        return;
    }
    console.log(selRows);

    var postData = "";
    $.each(selRows, function(i) {
        postData += this.id;
        if (i < selRows.length - 1) {
            postData += ",";
        }
    });
    $.confirm({
        title: '提示',
        content: '您确定要删除角色？',
        icon: 'mdi mdi-alert',
        animation: 'scale',
        closeAnimation: 'zoom',
        buttons: {
            confirm: {
                text: '是的',
                btnClass: 'btn-orange',
                action: function() {
                    $.ajax({
                        //几个参数需要注意一下
                        type: "POST",//方法类型
                        dataType: "json",//预期服务器返回的数据类型
                        url: "/admin/attach/del" ,//url
                        data: {id:'',ids: postData},
                        success: function (res) {
                            console.log(res);//打印服务端返回的数据(调试用)
                            if (res.code === 0) {
                                $.alert({
                                    title: '成功',
                                    icon: 'mdi mdi-alert',
                                    type: 'blue',
                                    content: res.msg,
                                    buttons: {
                                        okay: {
                                            text: '确认',
                                            btnClass: 'btn-blue',
                                            action: function() {
                                                $treeTable.bootstrapTable('refresh');
                                            }
                                        }
                                    }
                                });
                            }else {
                                $.alert({
                                    title: '错误',
                                    icon: 'mdi mdi-alert',
                                    type: 'red',
                                    content: res.msg,
                                    buttons: {
                                        okay: {
                                            text: '确认',
                                            btnClass: 'btn-blue',
                                            action: function() {
                                                //$('[alt="captcha"]').click();
                                            }
                                        }
                                    }
                                });
                            };
                        },
                        error : function() {
                            $.alert({
                                title: '警告',
                                content: '请检查返回是否正常！',
                                icon: 'mdi mdi-access-point-network-off',
                                animation: 'scale',
                                closeAnimation: 'scale',
                                buttons: {
                                    okay: {
                                        text: '确认',
                                        btnClass: 'btn-blue'
                                    }
                                }
                            });
                        }
                    });
                }
            },
            '取消': function() {

            }
        }
    });
}