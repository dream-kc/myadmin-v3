// tree-grid使用
var $treeTable = $('#tb_departments');
$treeTable.bootstrapTable({

    classes: 'table table-bordered table-hover table-striped',
    url: '/admin/oplog/datalist',
    method: 'get',
    dataType: 'json', // 因为本示例中是跨域的调用,所以涉及到ajax都采用jsonp,
    uniqueId: 'id',
    idField: 'id', // 每行的唯一标识字段
    toolbar: '#toolbar2', // 工具按钮容器
    //clickToSelect: true,     // 是否启用点击选中行
    showColumns: true, // 是否显示所有的列
    showToggle: true, // 是否显示详细视图和列表视图的切换按钮(clickToSelect同时设置为true时点击会报错)
    pagination: true, // 是否显示分页
    sortOrder: "asc", // 排序方式
    pageNumber: 1, // 初始化加载第一页，默认第一页
    pageSize: 10, // 每页的记录行数
    pageList: [10, 25, 50, 100], // 可供选择的每页的行数
    // 传递参数
    queryParams: function(params) {
        return {
            limit: params.limit, // 每页数据量
            offset: params.offset, // sql语句起始索引
            page: (params.offset / params.limit) + 1,
            sort: params.sort, // 排序的列名
            sortOrder: params.order // 排序方式'asc' 'desc'
        };
    },
    sidePagination: "client", // 分页方式：client客户端分页，server服务端分页

    columns: [
        {
            field: 'check',
            checkbox: true
        },
        {
            field: 'node',
            title: '操作权限',
            formatter: function(value, row, index) {
                return result = '<div><p style="color: #009688">操作账号：'+ row.username +'</p><p style="color: #999999">操作节点：'+ row.node +'</p></div>'
            },
        },
        {
            field: 'action',
            title: '操作行为',
            formatter: function(value, row, index) {
                return result = '<div><p style="color: #009688">'+ row.action +'</p><p style="color: #999999">'+ row.content +'</p></div>'
            },
        },
        {
            field: 'geoip',
            title: '地址位置'
        },
        {
            field: 'create_time',
            title: '创建时间'
        },
        {
            field: 'operate',
            title: '操作',
            align: 'center',
            events: {
                'click .role-delete': function(e, value, row, index) {
                    del(row.id);
                }
            },
            formatter: operateFormatter
        }
    ],

    responseHandler: function(res) {
        return {
            //"total": res.DATA.totalRecordCount,//总页数
            "rows": res.data   //数据
        };
    },

    treeShowField: 'name',
    parentIdField: 'pid',
    search:false,  //是否启用搜索框
    searchOnEnterKey:false,  //按回车触发搜索方法
    showRefresh: true,  //是否显示刷新按钮

});

// 操作按钮
function operateFormatter(value, row, index) {
    return [
        '<a type="button" class="role-delete btn btn-xs btn-default" title="删除" data-toggle="tooltip"><i class="mdi mdi-delete"></i></a>'
    ].join('');
}

/**
 * 选中父项时，同时选中子项
 * @param datas 所有的数据
 * @param row 当前数据
 * @param id id 字段名
 * @param pid 父id字段名
 */
function selectChilds(datas, row, id, pid, checked) {
    for (var i in datas) {
        if (datas[i][pid] == row[id]) {
            datas[i].check = checked;
            selectChilds(datas, datas[i], id, pid, checked);
        };
    }
}

function selectParentChecked(datas, row, id, pid) {
    for (var i in datas) {
        if (datas[i][id] == row[pid]) {
            datas[i].check = true;
            selectParentChecked(datas, datas[i], id, pid);
        };
    }
}


function del(id) {
    $.confirm({
        title: '提示',
        content: '您确定要删除此条日志？',
        icon: 'mdi mdi-alert',
        animation: 'scale',
        closeAnimation: 'zoom',
        buttons: {
            confirm: {
                text: '是的',
                btnClass: 'btn-orange',
                action: function() {
                    $.ajax({
                        //几个参数需要注意一下
                        type: "POST",//方法类型
                        dataType: "json",//预期服务器返回的数据类型
                        url: "/admin/oplog/del" ,//url
                        data: {id: id},
                        success: function (res) {
                            console.log(res);//打印服务端返回的数据(调试用)
                            if (res.code === 0) {
                                $.alert({
                                    title: '成功',
                                    icon: 'mdi mdi-alert',
                                    type: 'blue',
                                    content: res.msg,
                                    buttons: {
                                        okay: {
                                            text: '确认',
                                            btnClass: 'btn-blue',
                                            action: function() {
                                                $treeTable.bootstrapTable('refresh');
                                            }
                                        }
                                    }
                                });
                            }else {
                                $.alert({
                                    title: '错误',
                                    icon: 'mdi mdi-alert',
                                    type: 'red',
                                    content: res.msg,
                                    buttons: {
                                        okay: {
                                            text: '确认',
                                            btnClass: 'btn-blue',
                                            action: function() {
                                                //$('[alt="captcha"]').click();
                                            }
                                        }
                                    }
                                });
                            };
                        },
                        error : function() {
                            $.alert({
                                title: '警告',
                                content: '请检查返回是否正常！',
                                icon: 'mdi mdi-access-point-network-off',
                                animation: 'scale',
                                closeAnimation: 'scale',
                                buttons: {
                                    okay: {
                                        text: '确认',
                                        btnClass: 'btn-blue'
                                    }
                                }
                            });
                        }
                    });
                }
            },
            '取消': function() {

            }
        }
    });

}

function test() {
    var selRows = $treeTable.bootstrapTable("getSelections");
    if (selRows.length == 0) {
        alert("请至少选择一行");
        return;
    }
    console.log(selRows);

    var postData = "";
    $.each(selRows, function(i) {
        postData += this.id;
        if (i < selRows.length - 1) {
            postData += ",";
        }
    });
    $.confirm({
        title: '提示',
        content: '您确定要删除日志？',
        icon: 'mdi mdi-alert',
        animation: 'scale',
        closeAnimation: 'zoom',
        buttons: {
            confirm: {
                text: '是的',
                btnClass: 'btn-orange',
                action: function() {
                    $.ajax({
                        //几个参数需要注意一下
                        type: "POST",//方法类型
                        dataType: "json",//预期服务器返回的数据类型
                        url: "/admin/oplog/del" ,//url
                        data: {id:'',ids: postData},
                        success: function (res) {
                            console.log(res);//打印服务端返回的数据(调试用)
                            if (res.code === 0) {
                                $.alert({
                                    title: '成功',
                                    icon: 'mdi mdi-alert',
                                    type: 'blue',
                                    content: res.msg,
                                    buttons: {
                                        okay: {
                                            text: '确认',
                                            btnClass: 'btn-blue',
                                            action: function() {
                                                $treeTable.bootstrapTable('refresh');
                                            }
                                        }
                                    }
                                });
                            }else {
                                $.alert({
                                    title: '错误',
                                    icon: 'mdi mdi-alert',
                                    type: 'red',
                                    content: res.msg,
                                    buttons: {
                                        okay: {
                                            text: '确认',
                                            btnClass: 'btn-blue',
                                            action: function() {
                                                //$('[alt="captcha"]').click();
                                            }
                                        }
                                    }
                                });
                            };
                        },
                        error : function() {
                            $.alert({
                                title: '警告',
                                content: '请检查返回是否正常！',
                                icon: 'mdi mdi-access-point-network-off',
                                animation: 'scale',
                                closeAnimation: 'scale',
                                buttons: {
                                    okay: {
                                        text: '确认',
                                        btnClass: 'btn-blue'
                                    }
                                }
                            });
                        }
                    });
                }
            },
            '取消': function() {

            }
        }
    });
}