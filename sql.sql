/*
 Navicat MySQL Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : localhost:3306
 Source Schema         : sql_127_0_0_1

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 08/02/2022 23:38:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for my_admin
-- ----------------------------
DROP TABLE IF EXISTS `my_admin`;
CREATE TABLE `my_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `is_admin` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否超级管理员',
  `username` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户账号',
  `password` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户密码',
  `salt` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '加密钥',
  `nickname` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户昵称',
  `headimg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `phone` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '电话',
  `email` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '邮箱',
  `login_ip` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `login_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '登录时间',
  `login_num` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录次数',
  `last_login_ip` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '上次登录IP',
  `last_login_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '上次登录时间',
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'user_agent',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0禁用,1启用)',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_admin
-- ----------------------------
INSERT INTO `my_admin` VALUES (1, 1, 'admin', '7b12d0d85fa7906d8930a80b596c9dbf', 'C9MicVFvA51Gx83tUbkR', '管理员', '', '', '', '127.0.0.1', 1644376338, 58, '127.0.0.1', 1644337682, NULL, 1, 1606649826, 1644376338);
INSERT INTO `my_admin` VALUES (2, 0, 'demo', '2557341819f9c65ae6141a146d4d2a0b', 'VLwf2qglxTHKjQDvGneb', '体验账号', '', '', '', '127.0.0.1', 1644337783, 801, '127.0.0.1', 1644337783, NULL, 1, 1606649826, 1644376354);
INSERT INTO `my_admin` VALUES (18, 0, 'test', '91c81b847f3426275b8f189b304351a7', 'w3fmc7BPLyiOH6nAeu2F', '测试', '', '', '', NULL, NULL, 0, '', NULL, NULL, 1, 1628062269, 1644376365);


-- ----------------------------
-- Table structure for my_attachment
-- ----------------------------
DROP TABLE IF EXISTS `my_attachment`;
CREATE TABLE `my_attachment`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '管理员ID',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '物理路径',
  `filetype` char(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件类型',
  `filesize` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '文件大小',
  `mimetype` char(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'mime类型',
  `storage` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '存储位置',
  `sha1` char(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件 sha1编码',
  `hash` char(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '云存储hash(ETag)',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for my_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `my_auth_group`;
CREATE TABLE `my_auth_group`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `title` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `comments` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '角色描述',
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '角色所拥有的权限ID',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0禁用,1启用)',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_auth_group
-- ----------------------------
INSERT INTO `my_auth_group` VALUES (2, '普通用户', '普通用户', '4,5,8,9,10,33', 1, 1606650669, 1615447994);
INSERT INTO `my_auth_group` VALUES (3, '游客', '游客', '1,2,48,3,38,49,51,4,5,33,6,34,7,35', 1, 1606650669, 1616749330);

-- ----------------------------
-- Table structure for my_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `my_auth_group_access`;
CREATE TABLE `my_auth_group_access`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(10) UNSIGNED NOT NULL COMMENT '管理员ID',
  `group_id` mediumint(8) UNSIGNED NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限角色组关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_auth_group_access
-- ----------------------------
INSERT INTO `my_auth_group_access` VALUES (52, 1, 0);
INSERT INTO `my_auth_group_access` VALUES (65, 18, 3);
INSERT INTO `my_auth_group_access` VALUES (66, 0, 2);
INSERT INTO `my_auth_group_access` VALUES (67, 0, 3);
INSERT INTO `my_auth_group_access` VALUES (68, 2, 3);

-- ----------------------------
-- Table structure for my_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `my_auth_rule`;
CREATE TABLE `my_auth_rule`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '权限ID',
  `pid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
  `name` char(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否为有效规则',
  `ismenu` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '规则类型(0链接,1按钮)	',
  `isnav` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '导航菜单(0否,1是)',
  `icon` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单图标',
  `weight` mediumint(8) UNSIGNED NOT NULL DEFAULT 50 COMMENT '权重排序',
  `open` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '菜单(0收起,1展开)',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0禁用,1启用)',
  `condition` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '规则表达式',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限规则表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_auth_rule
-- ----------------------------
INSERT INTO `my_auth_rule` VALUES (1, 0, '', '系统配置', 0, 0, 1, 'mdi mdi-adobe-acrobat', 1, 1, 1, '', 1644328834, 1644329015);
INSERT INTO `my_auth_rule` VALUES (2, 0, '', '权限管理', 0, 0, 1, 'mdi mdi-air-conditioner', 2, 1, 1, '', 1644328865, 1644328876);
INSERT INTO `my_auth_rule` VALUES (3, 1, 'admin/configure/index', '参数配置', 1, 0, 1, '', 50, 1, 1, '', 1644332000, 1644332595);
INSERT INTO `my_auth_rule` VALUES (4, 1, 'admin/oplog/index', '日志管理', 1, 0, 1, '', 50, 1, 1, '', 1644332648, 1644332678);
INSERT INTO `my_auth_rule` VALUES (5, 1, 'admin/attach/index', '附件管理', 1, 0, 1, '', 50, 1, 1, '', 1644332746, 1644332746);
INSERT INTO `my_auth_rule` VALUES (6, 2, 'admin/admin/index', '用户管理', 1, 0, 1, '', 50, 1, 1, '', 1644332774, 1644332790);
INSERT INTO `my_auth_rule` VALUES (7, 2, 'admin/role/index', '角色管理', 1, 0, 1, '', 50, 1, 1, '', 1644332844, 1644332863);
INSERT INTO `my_auth_rule` VALUES (8, 2, 'admin/auth/index', '规则管理', 1, 0, 1, '', 50, 1, 1, '', 1644333083, 1644333365);
INSERT INTO `my_auth_rule` VALUES (9, 3, 'admin/configure/submit', '保存配置', 1, 1, 0, '', 50, 1, 1, '', 1644333560, 1644333787);
INSERT INTO `my_auth_rule` VALUES (10, 3, 'admin/upload/upload', '文件上传', 1, 1, 0, '', 50, 1, 1, '', 1644333665, 1644333779);
INSERT INTO `my_auth_rule` VALUES (11, 3, 'admin/configure/index', '查看', 1, 1, 0, '', 50, 1, 1, '', 1644333769, 1644333769);
INSERT INTO `my_auth_rule` VALUES (12, 3, 'admin/tpl/password', '修改密码', 1, 1, 0, '', 50, 1, 1, '', 1644333821, 1644333821);
INSERT INTO `my_auth_rule` VALUES (13, 4, 'admin/oplog/datalist', '查看', 1, 1, 0, '', 50, 1, 1, '', 1644333851, 1644333851);
INSERT INTO `my_auth_rule` VALUES (14, 4, 'admin/oplog/del', '删除', 1, 1, 0, '', 50, 1, 1, '', 1644333873, 1644333873);
INSERT INTO `my_auth_rule` VALUES (15, 4, 'admin/oplog/delall', '一键清空', 1, 1, 0, '', 50, 1, 1, '', 1644333898, 1644333898);
INSERT INTO `my_auth_rule` VALUES (16, 5, 'admin/attach/del', '删除', 1, 1, 0, '', 50, 1, 1, '', 1644333920, 1644333920);
INSERT INTO `my_auth_rule` VALUES (17, 5, 'admin/attach/datalist', '查看', 1, 1, 0, '', 50, 1, 1, '', 1644333939, 1644333939);
INSERT INTO `my_auth_rule` VALUES (18, 6, 'admin/admin/add', '添加', 1, 1, 0, '', 50, 1, 1, '', 1644333964, 1644333964);
INSERT INTO `my_auth_rule` VALUES (19, 6, 'admin/admin/edit', '修改', 1, 1, 0, '', 50, 1, 1, '', 1644333981, 1644333981);
INSERT INTO `my_auth_rule` VALUES (20, 6, 'admin/admin/del', '删除', 1, 1, 0, '', 50, 1, 1, '', 1644333999, 1644333999);
INSERT INTO `my_auth_rule` VALUES (21, 6, 'admin/admin/datalist', '查看', 1, 1, 0, '', 50, 1, 1, '', 1644334017, 1644334017);
INSERT INTO `my_auth_rule` VALUES (22, 7, 'admin/role/add', '添加', 1, 1, 0, '', 50, 1, 1, '', 1644334037, 1644334037);
INSERT INTO `my_auth_rule` VALUES (23, 7, 'admin/role/edit', '修改', 1, 1, 0, '', 50, 1, 1, '', 1644334056, 1644334056);
INSERT INTO `my_auth_rule` VALUES (24, 7, 'admin/role/del', '删除', 1, 1, 0, '', 50, 1, 1, '', 1644334074, 1644334074);
INSERT INTO `my_auth_rule` VALUES (25, 7, 'admin/role/authlist', '权限分配', 1, 1, 0, '', 50, 1, 1, '', 1644334099, 1644334099);
INSERT INTO `my_auth_rule` VALUES (26, 7, 'admin/role/datalist', '查看', 1, 1, 0, '', 50, 1, 1, '', 1644334116, 1644334116);
INSERT INTO `my_auth_rule` VALUES (27, 8, 'admin/auth/add', '添加', 1, 1, 0, '', 50, 1, 1, '', 1644334144, 1644334144);
INSERT INTO `my_auth_rule` VALUES (28, 8, 'admin/auth/edit', '修改', 1, 1, 0, '', 50, 1, 1, '', 1644334165, 1644334165);
INSERT INTO `my_auth_rule` VALUES (29, 8, 'admin/auth/del', '删除', 1, 1, 0, '', 50, 1, 1, '', 1644334184, 1644334184);
INSERT INTO `my_auth_rule` VALUES (30, 8, 'admin/auth/datalist', '查看', 1, 1, 0, '', 50, 1, 1, '', 1644334203, 1644334203);

-- ----------------------------
-- Table structure for my_config
-- ----------------------------
DROP TABLE IF EXISTS `my_config`;
CREATE TABLE `my_config`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分类',
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '配置值',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_system_config_type`(`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_config
-- ----------------------------
INSERT INTO `my_config` VALUES (1, 'system', '{\"typename\":\"system\",\"title\":\"MYadmin\",\"webname\":\"\",\"domain\":\"\",\"logo\":\"\",\"keywords\":\"\",\"description\":\"\",\"copyright\":\"\",\"miitbeian\":\"\"}');
INSERT INTO `my_config` VALUES (2, 'storage', '{\"typename\":\"storage\",\"engine\":\"1\",\"accesskey\":\"\",\"secretkey\":\"\",\"bucket\":\"\",\"domain\":\"\"}');
INSERT INTO `my_config` VALUES (3, 'weixin', NULL);
INSERT INTO `my_config` VALUES (4, 'wxapp', NULL);
INSERT INTO `my_config` VALUES (5, 'wxpay', NULL);
INSERT INTO `my_config` VALUES (6, 'email', '{\"typename\":\"email\",\"username\":\"\",\"fullname\":\"\",\"password\":\"\",\"host\":\"\",\"port\":\"\",\"subject\":\"\",\"body\":\"\",\"notice_email\":\"\"}');
INSERT INTO `my_config` VALUES (7, 'system1', NULL);

-- ----------------------------
-- Table structure for my_oplog
-- ----------------------------
DROP TABLE IF EXISTS `my_oplog`;
CREATE TABLE `my_oplog`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `node` char(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '当前操作节点',
  `geoip` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作者IP地址',
  `action` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作行为名称',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作内容描述',
  `username` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人用户名',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_oplog
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
